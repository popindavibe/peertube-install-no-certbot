Automatic installation of peertube on Debian
============================================

Peertube
--------
If you do not kwow what is peertube, see https://purr.rigelk.eu/lang/en/docs/getting-started/

Version of PeerTube
-------------------
This playbook has been tested for a **fresh** installation of PeerTube on version **v1.0.0-beta.12**.
Please contact us / open an issue if you think this is outdated and needs a refresh.

Playbook
--------
The `peertube` role installs all required packages and further downloads and install Peertube on a Debian server (>= Stretch).
This is just an automation of the official documentation to install Peertube on Debian that you can found at: https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md

The playbook will set up a minimal configuration for Peertube and Nginx in the scenario of being behind a reverse proxy (=> **no HTTPS** is done on the host).

Whenever needed, passwords are generated dynamically and stored in ~/%s.credentials.txt so that Ansible reuse them on next run (idempotent).

To start PeerTube service after installation, use:
```
systemctl start peertube
```

The password for the PeerTube **root** user is generated during the first startup. You can use the following command to filter it out:
```
journalctl -u peertube | egrep 'User password'
```

Requirements
------------

OS: Debian Stretch (tested successfully in LXC container)

Read **carefully** the output of the play as well as the official documentation. **Review and complete** the resulting configuration files in `/var/www/peertube/config/production.yaml`. 

PeerTube **does not support webserver host change**. Keep in mind your domain name is definitive after your first PeerTube start.

Role Variables
--------------

- **app_user**: user for running the peertube instance. Set to `peertube` as per documentation.
- **app_domain**: the FQDN for your resulting Peertube instance.

Dependencies
------------

Included role `nodejs` is imported during play to install nodejs from specific repo as per documentation.

Example
------- 

To run the play, set up **[peertube]** group in your inventory containing the host you wish to deploy peertube onto.

An example **hosts** file is provided. You can set your server name into the file and then run:
```
ansible-playbook peertube.yml -i hosts 
```

License
-------

GPLv3

Author Information
------------------

Nomagic is a UK-based member of the CHATONS collective.
